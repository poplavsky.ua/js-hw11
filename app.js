/*
Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
- У файлі index.html лежить розмітка двох полів вводу пароля.
- Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
- Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
- Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
- Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
- Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
- Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
- Після натискання на кнопку сторінка не повинна перезавантажуватись
- Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.
*/

let iconPass = document.querySelectorAll(".icon-password");
let inputConfirm = document.querySelector('.confirm');
let btnConfirm = document.querySelector('.btn');

iconPass.forEach(element => element.addEventListener('click', e => {
  const target = e.target;
  const inputPass = target.closest('label').querySelector('.js-password-input');
  inputPass.type === 'password' ? inputPass.type = 'text' : inputPass.type = 'password';
  target.classList.toggle('fa-eye-slash');
}))

btnConfirm.addEventListener ('click', e => {
  e.preventDefault();
  verificationPass();
})

function verificationPass () {
  let div;
  let pass1 = document.getElementById('password1');
  let pass2 = document.getElementById('password2');
  if (pass1.value === "" || pass2.value === "") {
    alert('Enter password');
  } else if (pass1.value === pass2.value) {
    alert('You are welcome');
  } else {
    div = document.createElement('div');
    div.style.color = 'red';
    div.textContent = "Потрібно ввести однакові значення";
    inputConfirm.after(div);
  }
  pass1.addEventListener('focus', () => div.textContent = "");
  pass2.addEventListener('focus', () => div.textContent = "");
}